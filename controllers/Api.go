package controllers

import (
	"github.com/astaxie/beego"
	models "gitlab.com/edot92/gogateway/models"
)

// APIController ...
type APIController struct {
	beego.Controller
}

// GetTags controller
func (c *APIController) GetTags() {
	type resss struct {
		name []string
	}
	r := models.GetTagsAPI()
	c.Data["json"] = &r
	c.ServeJSON()
}

// GetTags controller
func (c *APIController) GetTagsValue() {
	type responseTagsValue struct {
		name  string
		value string
	}
	var resJSON1 responseTagsValue

	in := c.Ctx.Input.Param(":name")
	if in == "" {
		resJSON1.name = ""
		resJSON1.value = ""
		c.Data["json"] = &resJSON1
		c.ServeJSON()
	} else {
		res := models.GetTagsValue(in)
		resJSON1.name = ""
		resJSON1.value = ""
		c.Data["json"] = &res
		c.ServeJSON()
	}

}
