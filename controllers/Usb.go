package controllers

import (
	"encoding/json"

		koneksi "gitlab.com/edot92/gogateway/koneksi"
	models "gitlab.com/edot92/gogateway/models"
	"github.com/astaxie/beego"
)

// UsbController ...
type UsbController struct {
	beego.Controller
}

// UsbController.URLMapping ...
func (c *UsbController) URLMapping() {
	c.Mapping("Getportavailable", c.Getportavailable)
	c.Mapping("Getportalias", c.Getportalias)
	c.Mapping("Updateportalias", c.Updateportalias)
}

// @router /getportavailable [get]
func (c *UsbController) Getportavailable() {
	var resUsbAvailabel []koneksi.Usbtersedias
	isOk, resUsbAvailabel, err := models.Getportavailable()
	if isOk == true {
		c.Data["json"] = resUsbAvailabel
		c.ServeJSON()
	} else {
		c.Data["json"] = resUsbAvailabel
		c.ServeJSON()
	}
	_ = err
}

// @router /getportalias [get]
func (c *UsbController) Getportalias() {
	var resUsbAlias []koneksi.Usbport
	isOk, resUsbAlias, err := models.Getportalias()
	if isOk == true {
		c.Data["json"] = resUsbAlias
		c.ServeJSON()
	} else {
		c.Data["json"] = resUsbAlias
		c.ServeJSON()
	}
	_ = err
}

// @router /updateportalias [post]
func (c *UsbController) Updateportalias() {
	// {"Data":{"ID":"1","UsbAlias":"PORT1","UsbSystem":"COM7"}}
	type paramArray struct {
		Data koneksi.Usbport
	}
	var ob paramArray
	json.Unmarshal(c.Ctx.Input.RequestBody, &ob)

	isOk, err := models.Updateportalias(ob.Data)
	if isOk == true {
		c.Data["json"] = map[string]interface{}{"Status": "success", "Message": "Berhasil Mengupdate"}
		c.ServeJSON()
	} else {
		c.Data["json"] = map[string]interface{}{"Status": "warning", "Message": "Gagal Mengupdate " + err[0].Error()}
		c.ServeJSON()
	}
	_ = err
}
