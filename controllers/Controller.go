package controllers

import (
	"encoding/json"
	"fmt"
	"strconv"
	"strings"

	"gitlab.com/edot92/gogateway/models"

	koneksi "gitlab.com/edot92/gogateway/koneksi"

	"github.com/astaxie/beego"
)

type responseWeb struct {
	Data    string
	Message string
	Status  int
	Error   string
}
type responseController struct {
	Data    *[]koneksi.ControllerList
	Message string
	Status  int
	Error   string
}

// ControllerController ...
type ControllerController struct {
	beego.Controller
}

// ControllerController.URLMapping ...
func (c *ControllerController) URLMapping() {

	c.Mapping("Addnewcontroller", c.Addnewcontroller)
	c.Mapping("Gettablecontroller", c.Gettablecontroller)
	c.Mapping("Deletecontroller", c.Deletecontroller)
	c.Mapping("Updatecontroller", c.Updatecontroller)
	c.Mapping("Updatecontrollermultiplecontroller", c.Updatecontrollermultiplecontroller)

}

// @Title Addnewcontroller
// @Description API Membuat Tag Baru
// @Default models.Controller.ControllerListForAddNew
// @Param body body models.Controller.ControllerListForAddNew false "body for user content"
// @Success 200 {object} responseWeb
// @Failure 403 :  empty data
// @router /addnewcontroller [get]
func (c *ControllerController) Addnewcontroller() {

	var ControllerList koneksi.ControllerList
	c.Ctx.Input.Bind(&ControllerList.IdnameController, "idname_controller")
	c.Ctx.Input.Bind(&ControllerList.IdactiveController, "idactive_controller")
	c.Ctx.Input.Bind(&ControllerList.IdcommunicationmodeController, "idcommunicationmode_controller")
	c.Ctx.Input.Bind(&ControllerList.IdstationController, "idstation_controller")
	c.Ctx.Input.Bind(&ControllerList.IdprotocolController, "idprotocol_controller")
	c.Ctx.Input.Bind(&ControllerList.IdportController, "idport_controller")
	c.Ctx.Input.Bind(&ControllerList.IdbaudController, "idbaud_controller")
	c.Ctx.Input.Bind(&ControllerList.IdparityController, "idparity_controller")
	c.Ctx.Input.Bind(&ControllerList.IddatabitController, "iddatabit_controller")
	c.Ctx.Input.Bind(&ControllerList.IdstopbitsController, "idstopbits_controller")
	ControllerList.IdnameController = strings.ToUpper(ControllerList.IdnameController)
	fmt.Println("IdnameController = ", ControllerList.IdnameController)
	if ControllerList.IdnameController == "" {
		var respon responseWeb
		respon.Error = "ISI NAMA CONTROLLER "
		respon.Message = "failed"
		respon.Status = 211
		// fmt.Println("respon", respon)
		// {"data":"","message":"failed","tittle":"","status":200,"error":"idstation_controller 1 atau idname_controller sudah tersedia"}
		c.Data["json"] = &respon
		c.ServeJSON()
		return
	}
	isOk, err := models.Addnewcontroller(ControllerList)
	if isOk == true {
		// {"data":"berhasil update ke database","message":"success","tittle":"","status":200}

		var respon responseWeb
		respon.Data = "berhasil update ke database"
		respon.Message = "success"
		respon.Status = 201
		// fmt.Println("respon", respon)
		c.Data["json"] = &respon
		c.ServeJSON()
	} else {

		var respon responseWeb
		respon.Error = err.Error()
		respon.Message = "failed"
		respon.Status = 210
		// fmt.Println("respon", respon)
		// {"data":"","message":"failed","tittle":"","status":200,"error":"idstation_controller 1 atau idname_controller sudah tersedia"}
		c.Data["json"] = &respon
		c.ServeJSON()
	}
}

// @router /gettablecontroller [get]
func (c *ControllerController) Gettablecontroller() {
	isOk, res, err := models.Gettablecontroller()
	if isOk == true {
		// {"data":"berhasil update ke database","message":"success","tittle":"","status":200}
		var respon responseController

		respon.Data = &res
		respon.Message = "success"
		respon.Status = 201
		c.Data["json"] = &respon
		c.ServeJSON()
	} else {
		var respon responseWeb

		respon.Error = err[0].Error()
		respon.Message = "failed"
		respon.Status = 210
		// fmt.Println("respon", respon)
		// {"data":"","message":"failed","tittle":"","status":200,"error":"idstation_controller 1 atau idname_controller sudah tersedia"}
		c.Data["json"] = &respon
		c.ServeJSON()
	}
}

// @router /deletecontroller [get]
func (c *ControllerController) Deletecontroller() {
	var ControllerList koneksi.ControllerList
	c.Ctx.Input.Bind(&ControllerList.IdnameController, "IdnameController")
	c.Ctx.Input.Bind(&ControllerList.ID, "ID")
	tagYangDidelete := ControllerList.IdnameController
	idYangDidelete := ControllerList.ID
	if idYangDidelete == 0 {
		var respon responseWeb
		respon.Data = "berhasil menghapus tag " + tagYangDidelete + " :)"
		respon.Message = "success"
		respon.Status = 201
		c.Data["json"] = &respon
		c.ServeJSON()
	}
	isOk, err := models.Deletecontroller(ControllerList)
	if isOk == true {
		var respon responseWeb
		respon.Data = "berhasil menghapus tag " + tagYangDidelete
		respon.Message = "success"
		respon.Status = 201
		c.Data["json"] = &respon
		c.ServeJSON()
	} else {
		var respon responseWeb
		fmt.Println(err)
		respon.Error = err[0].Error()
		respon.Message = "failed"
		respon.Status = 210
		// fmt.Println("respon", respon)
		// {"data":"","message":"failed","tittle":"","status":200,"error":"idstation_controller 1 atau idname_controller sudah tersedia"}
		c.Data["json"] = &respon
		c.ServeJSON()
	}
}

// @router /updatecontroller [post]
func (c *ControllerController) Updatecontroller() {
	type paramArray struct {
		Data koneksi.ControllerList
	}
	var ControllerList paramArray
	json.Unmarshal(c.Ctx.Input.RequestBody, &ControllerList)
	tagYangUbah := ControllerList.Data.IdnameController
	idYangUbah := ControllerList.Data.ID
	if idYangUbah == 0 {
		var respon responseWeb
		respon.Data = "gagal mengupdate tag " + tagYangUbah + " :)"
		respon.Message = "success"
		respon.Status = 201
		c.Data["json"] = &respon
		c.ServeJSON()
		return
	}

	ControllerList.Data.IdnameController = strings.ToUpper(ControllerList.Data.IdnameController)
	isOk, err := models.Updatecontroller(ControllerList.Data)
	if isOk == true {
		var respon responseWeb
		respon.Data = "berhasil Mengupdate tag " + tagYangUbah
		respon.Message = "success"
		respon.Status = 201
		c.Data["json"] = &respon
		c.ServeJSON()
	} else {
		var respon responseWeb

		respon.Error = err[0].Error()
		respon.Message = "failed"
		respon.Status = 210
		// fmt.Println("respon", respon)
		// {"data":"","message":"failed","tittle":"","status":200,"error":"idstation_controller 1 atau idname_controller sudah tersedia"}
		c.Data["json"] = &respon
		c.ServeJSON()
	}
}

// @router /updatecontrollermultiplecontroller [post]
func (c *ControllerController) Updatecontrollermultiplecontroller() {
	type paramArray struct {
		Qty  uint
		Data []koneksi.ControllerList
	}
	var ob paramArray
	json.Unmarshal(c.Ctx.Input.RequestBody, &ob)

	// fmt.Println(ob.Qty)
	// loop update berdasarkan QTY
	var gagal uint
	gagal = 0
	var listTagSukes []string
	for i := uint(0); i < ob.Qty; i++ {
		var ControllerList koneksi.ControllerList
		ControllerList = ob.Data[i]
		fmt.Println("ID=  ", ControllerList.ID)
		ControllerList.IdnameController = strings.ToUpper(ControllerList.IdnameController)
		isOk, err := models.Updatecontroller(ControllerList)
		if isOk == false {
			gagal++
		} else {

			listTagSukes = append(listTagSukes, ControllerList.IdnameController)
		}
		_ = err

	}
	if gagal == 0 {
		// c.Data["json"] = map[string]interface{}{"respon": ob}
		var respon responseWeb
		respon.Error = ""
		respon.Message = "success"
		respon.Status = 201
		strings.Join(listTagSukes, respon.Data)
		c.Data["json"] = &respon
		c.ServeJSON()
	} else {
		var respon responseWeb
		respon.Error = "failed : " + strconv.Itoa(int(gagal)) + " update"
		respon.Message = "failed"
		respon.Status = 210
		c.Data["json"] = &respon
		c.ServeJSON()
	}

}
