package controllers

import (
	"encoding/json"

	models "gitlab.com/edot92/gogateway/models"

	koneksi "gitlab.com/edot92/gogateway/koneksi"

	"github.com/astaxie/beego"
)

func (c *TagController) URLMapping() {
	c.Mapping("Gettabletags", c.Gettabletags)
	c.Mapping("Buattagbaru", c.Buattagbaru)
	c.Mapping("Updatetagbyselectrow", c.Updatetagbyselectrow)
	c.Mapping("Deletetabletags", c.Deletetabletags)

}

// TagController ...
type TagController struct {
	beego.Controller
}

type response struct {
	ControllerList []string          `json:"controller_list"`
	TagList        []koneksi.TagList `json:"tag_list"`
	Message        string            `json:"message"`
	Status         int               `json:"status"`
	Error          string            `json"error"`
}

// @router /gettabletags [get]
func (c *TagController) Gettabletags() {

	res, err := models.GetTabletags()
	res2, err := models.GetNameController()
	_ = err
	res3 := response{
		TagList:        res,
		ControllerList: res2,
		Message:        "ok",
		Status:         200,
		Error:          "",
	}
	c.Data["json"] = res3
	c.ServeJSON()
}

// @router /buattagbaru [post]
func (c *TagController) Buattagbaru() {
	type paramJSON struct {
		Data koneksi.TagList
	}
	var ob paramJSON
	json.Unmarshal(c.Ctx.Input.RequestBody, &ob)
	if ob.Data.IdnameTag == "" {
		c.Data["json"] = map[string]interface{}{"Status": "warning", "Message": "Gagal Menambahkan ", "Error:": "ID tidak Tersedia"}
		c.ServeJSON()
	}
	isOk, err := models.Buattagbaru(ob.Data)
	if isOk == true {
		c.Data["json"] = map[string]interface{}{"Status": "success", "Message": "Berhasil Menambahkan Tag" + ob.Data.IdnameTag}
		c.ServeJSON()
	} else {
		c.Data["json"] = map[string]interface{}{"Status": "warning", "Message": "Gagal Menambahkan TAG: " + ob.Data.IdnameTag, "Error": err[0].Error()}
		c.ServeJSON()
	}
}

// @router /updatetagbyselectrow [post]
func (c *TagController) Updatetagbyselectrow() {
	type paramJSON struct {
		Data koneksi.TagList
	}

	var ob paramJSON
	json.Unmarshal(c.Ctx.Input.RequestBody, &ob)
	isOk, err := models.Updatetagbyselectrow(ob.Data)
	if isOk == true {
		c.Data["json"] = map[string]interface{}{"Status": "success", "Message": "Berhasil Mengupdate"}
		c.ServeJSON()
	} else {
		c.Data["json"] = map[string]interface{}{"Status": "warning", "Message": "Gagal Mengupdate " + err[0].Error()}
		c.ServeJSON()
	}
	_ = err
}

// @router /deletetabletags [post]
func (c *TagController) Deletetabletags() {
	type paramJSON struct {
		Data koneksi.TagList
	}
	var ob paramJSON
	json.Unmarshal(c.Ctx.Input.RequestBody, &ob)
	if ob.Data.ID < 1 {
		c.Data["json"] = map[string]interface{}{"Status": "warning", "Message": "Gagal Mengupdate " + ob.Data.IdnameTag, "Error:": "ID tidak Tersedia"}
		c.ServeJSON()
	}
	isOk, err := models.Deletetabletags(ob.Data)
	if isOk == true {
		c.Data["json"] = map[string]interface{}{"Status": "success", "Message": "Berhasil Menghapus Tag" + ob.Data.IdnameTag}
		c.ServeJSON()
	} else {
		c.Data["json"] = map[string]interface{}{"Status": "warning", "Message": "Gagal Mengupdate " + ob.Data.IdnameTag, "Error": err[0].Error()}
		c.ServeJSON()
	}
}
