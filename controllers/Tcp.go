package controllers

import (
	models "gitlab.com/edot92/gogateway/models"

	"github.com/astaxie/beego"
)

// TcepekController ...
type TcepekController struct {
	beego.Controller
}

// TcepekController.URLMapping ...
func (c *TcepekController) URLMapping() {
	c.Mapping("Getmodbustcpsetting", c.Getmodbustcpsetting)

}

// @router /getmodbustcpsetting [get]
func (c *TcepekController) Getmodbustcpsetting() {
	isOk, res, err := models.Getmodbustcpsetting()
	if isOk == true {
		c.Data["json"] = map[string]interface{}{"Status": "success", "Message": "", "Data": res}
		c.ServeJSON()
	} else {
		c.Data["json"] = map[string]interface{}{"Status": "warning", "Message": "Gagal Request ke Database ", "Error": err[0].Error()}
		c.ServeJSON()
	}

}
