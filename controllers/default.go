package controllers

import (
	"bytes"
	"encoding/json"
	"fmt"
	"strconv"

	"gitlab.com/edot92/gogateway/views"

	koneksi "gitlab.com/edot92/gogateway/koneksi"

	"github.com/astaxie/beego"
)

// MainController ...
type MainController struct {
	beego.Controller
}

// MainController.Get ...
func (c *MainController) Get() {

	// var Data []string
	listBaud := []string{"300", "600", "1200", "2400", "4800", "9600", "19200", "38400", "57600", "115200"}
	var listPort []string
	for i := 0; i < 4; i++ {
		listPort = append(listPort, "PORT"+strconv.Itoa(i+1))
	}
	var ipku string
	ipku = "http://" + c.Ctx.Input.Host() + ":" + strconv.Itoa(c.Ctx.Input.Port())
	fmt.Println(ipku)
	listParity := []string{"None", "Even", "Odd"}
	listDatabits := []string{"7", "8", "9"}
	listStopbits := []string{"1", "2"}
	var renderData koneksi.RenderdataHTML
	renderData.BaseURL = ipku
	renderData.ListBaud = listBaud
	renderData.ListPort = listPort
	renderData.ListParity = listParity
	renderData.ListDatabits = listDatabits
	renderData.ListStopbits = listStopbits
	renderData.Email = "edyprasetiyoo@gmail.com"
	renderData.Handphone = "083876989317"
	renderData.PORTModbusTCP = beego.AppConfig.String("PORT_MODBUS_TCP")
	renderJSONString, err := json.Marshal(renderData)
	if err != nil {
		panic(err)
	}
	buffer := new(bytes.Buffer)
	template.HalamanVar(renderJSONString, buffer)
	c.Ctx.ResponseWriter.Write(buffer.Bytes())
	c.Ctx.Output.Header("Access-Control-Allow-Origin", "*")
	c.Ctx.Output.Header("text/html", "charset=utf-8")
	
}
