package models

import (
	"errors"
	"strings"
	"time"

	koneksi "gitlab.com/edot92/gogateway/koneksi"
)

// Getportavailable ...
func Getportavailable() (bool, []koneksi.Usbtersedias, []error) {
	var resUsbAvailabel []koneksi.Usbtersedias
	err := koneksi.Condb.Find(&resUsbAvailabel).GetErrors()
	if len(err) > 0 {
		return false, nil, err
	}
	return true, resUsbAvailabel, nil
}

// Getportalias ...
func Getportalias() (bool, []koneksi.Usbport, []error) {
	var resUsbAvailabel []koneksi.Usbport
	err := koneksi.Condb.Find(&resUsbAvailabel).GetErrors()
	if len(err) > 0 {
		return false, nil, err
	}
	return true, resUsbAvailabel, nil
}

// Updateportalias ...
func Updateportalias(param koneksi.Usbport) (bool, []error) {
	// log.Fatal("datanya :", param.UsbAlias)
	// os.Exit(1)
	stringOk := []string{"PORT1", "PORT2", "PORT3", "PORT4"}
	lanjut := false
	for index := 0; index < len(stringOk); index++ {
		if strings.Contains(param.UsbAlias, stringOk[index]) == true {
			lanjut = true
		}
	}
	if lanjut == false {
		var errr []error
		errr = append(errr, errors.New("USB ALIAS PORT TIDAK SESUAI"))
		return false, errr
	}
	param.UpdatedAt = time.Now()
	err := koneksi.Condb.Model(&param).Where("usb_alias = ?", param.UsbAlias).Update("usb_system", param.UsbSystem).Update("updated_at", time.Now()).GetErrors()
	// err := koneksi.Condb.Model(&param).Updates(map[string]interface{}{"usb_system": param.UsbSystem,
	// 	"usb_alias": param.UsbAlias,
	// 	"update_at": time.Now()}).GetErrors()
	if len(err) > 0 {
		return false, err
	}
	return true, nil
}
