package models

import (
	"errors"
	"fmt"
	"time"

	koneksi "gitlab.com/edot92/gogateway/koneksi"
)

// ControllerListForAddNew ...
type ControllerListForAddNew struct {
	IdnameController              string `json:"required" doc:"controller satu"`
	IdactiveController            bool
	IdcommunicationmodeController string
	IdprotocolController          string
	IdstationController           int
	IdportController              string
	IdbaudController              string
	IdparityController            string
	IddatabitController           string
	IdstopbitsController          string
}

// Addnewcontroller ....
func Addnewcontroller(param koneksi.ControllerList) (bool, error) {
	var res koneksi.ControllerList
	err := koneksi.Condb.First(&res, "idname_controller = ?,updated_at", param.IdnameController, time.Now()).GetErrors()
	if res.IdnameController == "" {
		err = koneksi.Condb.Create(&param).GetErrors()

		if len(err) > 0 {
			return false, nil
		}
		fmt.Println("errornya:", err)
		return true, nil
	}
	return false, errors.New("idstation_controller 1 atau idname_controller sudah tersedia")
}

// Gettablecontroller ...
func Gettablecontroller() (bool, []koneksi.ControllerList, []error) {
	var res []koneksi.ControllerList
	err := koneksi.Condb.Find(&res).GetErrors()

	if len(err) > 0 {
		return false, nil, err
	}
	return true, res, nil
}

// Deletecontroller ...
func Deletecontroller(param koneksi.ControllerList) (bool, []error) {
	var q string
	var res koneksi.ControllerList
	err := koneksi.Condb.Where("id=?", param.ID).Find(&res).GetErrors()
	if len(err) > 0 {
		fmt.Println("QTY error", len(err))
		return false, err
	}

	if res.ID > 0 {

		q = "DELETE FROM controller_list WHERE idname_controller=?"
		// err := koneksi.Condb.Raw(q, param.IdnameController).GetErrors()
		err := koneksi.Condb.Delete(koneksi.ControllerList{}, "idname_controller=?", param.IdnameController).GetErrors()

		if len(err) > 0 {
			return false, err
		}
		q = "UPDATE tag_list SET idnametocontroller_tags='', idnamefromcontroller_tags='' WHERE idnamefromcontroller_tags =" + param.IdnameController + " OR idnametocontroller_tags=" + param.IdnameController
		koneksi.Condb.Raw(q).GetErrors()
		if len(err) > 0 {
			return false, err
		}
		return true, nil

	}
	var errr []error
	errr[0] = errors.New("TAG TIDAK DI TEMUKAN")
	return false, errr

}

// Updatecontroller ....
func Updatecontroller(param koneksi.ControllerList) (bool, []error) {
	var err []error

	if koneksi.TypeDatabase == "sqlite3" {
		if param.IdactiveController == false {
			err = koneksi.Condb.Model(&param).Updates(map[string]interface{}{
				"idname_controller":              param.IdnameController,
				"idactive_controller":            0,
				"idcommunicationmode_controller": param.IdcommunicationmodeController,
				"idprotocol_controller":          param.IdprotocolController,
				"idstation_controller":           param.IdstationController,
				"idport_controller":              param.IdportController,
				"idbaud_controller":              param.IdbaudController,
				"idparity_controller":            param.IdparityController,
				"iddatabit_controller":           param.IddatabitController,
				"idstopbits_controller":          param.IdstopbitsController,
				"update_at":                      time.Now()}).GetErrors()
		} else if param.IdactiveController == true {
			err = koneksi.Condb.Model(&param).Updates(map[string]interface{}{
				"idname_controller":              param.IdnameController,
				"idactive_controller":            1,
				"idcommunicationmode_controller": param.IdcommunicationmodeController,
				"idprotocol_controller":          param.IdprotocolController,
				"idstation_controller":           param.IdstationController,
				"idport_controller":              param.IdportController,
				"idbaud_controller":              param.IdbaudController,
				"idparity_controller":            param.IdparityController,
				"iddatabit_controller":           param.IddatabitController,
				"idstopbits_controller":          param.IdstopbitsController,
				"update_at":                      time.Now()}).GetErrors()
		}
	} else {
		err = koneksi.Condb.Model(&param).Updates(map[string]interface{}{"idname_controller": param.IdnameController,
			"idactive_controller":            param.IdactiveController,
			"idcommunicationmode_controller": param.IdcommunicationmodeController,
			"idprotocol_controller":          param.IdprotocolController,
			"idstation_controller":           param.IdstationController,
			"idport_controller":              param.IdportController,
			"idbaud_controller":              param.IdbaudController,
			"idparity_controller":            param.IdparityController,
			"iddatabit_controller":           param.IddatabitController,
			"idstopbits_controller":          param.IdstopbitsController,
			"update_at":                      time.Now()}).GetErrors()
	}

	if len(err) > 0 {
		return false, err
	}
	return true, nil
}

// Updatecontrollermultiplecontroller ...
func Updatecontrollermultiplecontroller(param koneksi.ControllerList) (bool, []error) {

	return true, nil
}
