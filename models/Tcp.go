package models

import (
	koneksi "gitlab.com/edot92/gogateway/koneksi"
)

// Gettcpsetting ....
func Getmodbustcpsetting() (bool, koneksi.OptionWebApi, []error) {
	var res koneksi.OptionWebApi
	err := koneksi.Condb.First(&res).GetErrors()
	if res.Port == 0 {
		return false, res, err
	}
	return true, res, nil

}
