package models

import (
	"errors"
	"fmt"
	"log"
	"strconv"
	"time"

	"github.com/astaxie/beego"
	"gitlab.com/edot92/gogateway/koneksi"
)

func init() {

}

// GetListTagsFromSelfDB mendapatkan list tags dari table tags
func GetListTagsFromSelfDB() ([]koneksi.TagList, error) {

	var res []koneksi.TagList
	koneksi.Condb.Find(&res)

	if len(res) > 0 {
		return res, nil
	}

	return nil, errors.New("data null atau error")
}

// UpdateToOtherDb param data dari table taglist
func UpdateToOtherDb(params []koneksi.TagList) error {
	qty := len(params)
	var query string
	namaTableOther := beego.AppConfig.String("DB_NAMA_TABLE_OTHERDB")
	deviceId, _ := beego.AppConfig.Int("DEVICE_ID_OTHERDB")
	query = "INSERT INTO " + namaTableOther + "("
	// get value and build string
	for i := 0; i < qty; i++ {
		if i < qty-1 {
			query = query + params[i].IdnameTag + ","
		} else {
			query = query + params[i].IdnameTag + ",date_created,device_id) VALUES("
		}
	}
	var datetime = time.Now().Format(time.RFC3339)
	// get value and build string
	for i := 0; i < qty; i++ {
		if i < qty-1 {
			query = query + params[i].IdlastvalueTag + ","
		} else {
			query = query + params[i].IdlastvalueTag + ",'" + datetime + "'," + strconv.Itoa(deviceId) + ") "
		}
	}
	err := koneksi.ConOtherdb.Exec(query).GetErrors()

	log.Println("query:", query)

	if len(err) > 0 {
		fmt.Println("ERROR OTHER DB UPDATE:", err)
		log.Println("ERROR OTHER DB UPDATE:", err)

		return errors.New(err[0].Error())
	}

	return nil
}
