package models

import (
	"fmt"
	"time"

	koneksi "gitlab.com/edot92/gogateway/koneksi"
)

//  GetTags dari model Taglist
func GetTagsAPI() []string {
	res, _ := GetTabletags()
	var resData []string
	fmt.Println(res[0].IdnameTag)
	count := len(res)
	for i := 0; i < count; i++ {
		resData = append(resData, res[i].IdnameTag)
	}
	fmt.Println(resData)
	// var temp []string
	return resData
}

//  ResponseGetTagsValue ...
type ResponseGetTagsValue struct {
	IdnameTag      string    `json:"name"`
	IdlastvalueTag string    `json:"value"`
	UpdatedAt      time.Time `json:"update"`
}

func GetTagsValue(paramName string) ResponseGetTagsValue {

	var result ResponseGetTagsValue
	err := koneksi.Condb.Debug().Raw("select idname_tag,idlastvalue_tag,updated_at from tag_lists where idname_tag=?", paramName).Scan(&result).GetErrors()
	if len(err) > 0 {

	}

	return result
}
