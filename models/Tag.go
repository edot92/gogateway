package models

import (
	"errors"
	"fmt"
	"time"

	koneksi "gitlab.com/edot92/gogateway/koneksi"
)

func GetTabletags() ([]koneksi.TagList, error) {

	var res []koneksi.TagList
	koneksi.Condb.Find(&res)
	if len(res) > 0 {
		return res, nil
	}
	return nil, errors.New("data null atau error")
}

func GetNameController() ([]string, error) {

	var res []koneksi.ControllerList
	var names []string
	koneksi.Condb.Find(&res).Pluck("idname_controller", &names)
	if len(res) > 0 {
		return names, nil
	}
	return nil, errors.New("data null atau error")
}

// Buattagbaru ....
func Buattagbaru(param koneksi.TagList) (bool, []error) {
	var resFind koneksi.TagList
	koneksi.Condb.Where("idname_tag = ?", param).First(&resFind)
	if resFind.IdnameTag == "" {
		err := koneksi.Condb.Table("tag_lists").Exec("INSERT INTO tag_lists (idname_tag) VALUES (?)", param.IdnameTag).GetErrors()
		if len(err) > 0 {
			return false, err
		}
		return true, nil
	}
	var errr []error
	errr[0] = errors.New("NAMA TAGS SUDAH ADA")
	return false, errr
}

//  Updatetagbyselectrow ...
func Updatetagbyselectrow(param koneksi.TagList) (bool, []error) {
	// IdnameTag=Tags117316142619172
	// IddatatypeTag=UINT16BE
	// IdaccessrightTag=WRITE
	// IdnamefromcontrollerTag=POWER%20METER%201%20PHASE
	// IdregisterfromcontrollerTag=40001
	// IdnametocontrollerTag=SELF_CONTROLLER
	// IdregistertocontrollerTag=40001
	err := koneksi.Condb.Model(&param).Where("ID = ?", param.ID).Update("idname_tag", param.IdnameTag).Update("iddatatype_tag", param.IddatatypeTag).Update("idaccessright_tag", param.IdaccessrightTag).Update("idnamefromcontroller_tag", param.IdnamefromcontrollerTag).Update("idregisterfromcontroller_tag", param.IdregisterfromcontrollerTag).Update("idnametocontroller_tag", param.IdnametocontrollerTag).Update("idregistertocontroller_tag", param.IdregistertocontrollerTag).Update("updated_at", time.Now()).GetErrors()
	if len(err) > 0 {
		return false, err
	}
	return true, nil
}

// deletetabletags ...
func Deletetabletags(param koneksi.TagList) (bool, []error) {
	var res koneksi.TagList
	err := koneksi.Condb.Where("id=?", param.ID).Find(&res).GetErrors()
	if len(err) > 0 {
		fmt.Println("QTY error", len(err))
		return false, err
	}
	if res.ID > 0 {

		err := koneksi.Condb.Delete(koneksi.TagList{}, "idname_tag=?", param.IdnameTag).GetErrors()
		if len(err) > 0 {
			return false, err
		}
		return true, nil

	}
	var errr []error
	errr[0] = errors.New("TAG TIDAK DI TEMUKAN")
	return false, errr

}
