// @APIVersion 1.0.0
// @Title JPA GATEWAY API
// @Description API Untuk Akses Ke Gateway
// @Contact edyprasetiyoo@gmail.com

package routers

import (
	"gitlab.com/edot92/gogateway/controllers"

	"github.com/astaxie/beego"
)

func init() {

	beego.Router("/", &controllers.MainController{})
	beego.Router("/Tags", &controllers.APIController{}, "get:GetTags")
	beego.Router("/tags/:name", &controllers.APIController{}, "get,put:GetTagsValue")
	ns := beego.NewNamespace("/web",
		beego.NSNamespace("/tagmodule",
			beego.NSInclude(
				&controllers.TagController{},
			),
		),
		beego.NSNamespace("/controllermodule",
			beego.NSInclude(
				&controllers.ControllerController{},
			),
		),
		beego.NSNamespace("/usbportmodule",
			beego.NSInclude(
				&controllers.UsbController{},
			),
		),
		beego.NSNamespace("/tcpmodule",
			beego.NSInclude(
				&controllers.TcepekController{},
			),
		),
	)
	beego.AddNamespace(ns)
}
