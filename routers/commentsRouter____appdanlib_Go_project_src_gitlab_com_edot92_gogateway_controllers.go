package routers

import (
	"github.com/astaxie/beego"
)

func init() {

	beego.GlobalControllerRouter["gitlab.com/edot92/gogateway/controllers:ControllerController"] = append(beego.GlobalControllerRouter["gitlab.com/edot92/gogateway/controllers:ControllerController"],
		beego.ControllerComments{
			Method: "Addnewcontroller",
			Router: `/addnewcontroller`,
			AllowHTTPMethods: []string{"get"},
			Params: nil})

	beego.GlobalControllerRouter["gitlab.com/edot92/gogateway/controllers:ControllerController"] = append(beego.GlobalControllerRouter["gitlab.com/edot92/gogateway/controllers:ControllerController"],
		beego.ControllerComments{
			Method: "Gettablecontroller",
			Router: `/gettablecontroller`,
			AllowHTTPMethods: []string{"get"},
			Params: nil})

	beego.GlobalControllerRouter["gitlab.com/edot92/gogateway/controllers:ControllerController"] = append(beego.GlobalControllerRouter["gitlab.com/edot92/gogateway/controllers:ControllerController"],
		beego.ControllerComments{
			Method: "Deletecontroller",
			Router: `/deletecontroller`,
			AllowHTTPMethods: []string{"get"},
			Params: nil})

	beego.GlobalControllerRouter["gitlab.com/edot92/gogateway/controllers:ControllerController"] = append(beego.GlobalControllerRouter["gitlab.com/edot92/gogateway/controllers:ControllerController"],
		beego.ControllerComments{
			Method: "Updatecontroller",
			Router: `/updatecontroller`,
			AllowHTTPMethods: []string{"post"},
			Params: nil})

	beego.GlobalControllerRouter["gitlab.com/edot92/gogateway/controllers:ControllerController"] = append(beego.GlobalControllerRouter["gitlab.com/edot92/gogateway/controllers:ControllerController"],
		beego.ControllerComments{
			Method: "Updatecontrollermultiplecontroller",
			Router: `/updatecontrollermultiplecontroller`,
			AllowHTTPMethods: []string{"post"},
			Params: nil})

	beego.GlobalControllerRouter["gitlab.com/edot92/gogateway/controllers:TagController"] = append(beego.GlobalControllerRouter["gitlab.com/edot92/gogateway/controllers:TagController"],
		beego.ControllerComments{
			Method: "Gettabletags",
			Router: `/gettabletags`,
			AllowHTTPMethods: []string{"get"},
			Params: nil})

	beego.GlobalControllerRouter["gitlab.com/edot92/gogateway/controllers:TagController"] = append(beego.GlobalControllerRouter["gitlab.com/edot92/gogateway/controllers:TagController"],
		beego.ControllerComments{
			Method: "Buattagbaru",
			Router: `/buattagbaru`,
			AllowHTTPMethods: []string{"post"},
			Params: nil})

	beego.GlobalControllerRouter["gitlab.com/edot92/gogateway/controllers:TagController"] = append(beego.GlobalControllerRouter["gitlab.com/edot92/gogateway/controllers:TagController"],
		beego.ControllerComments{
			Method: "Updatetagbyselectrow",
			Router: `/updatetagbyselectrow`,
			AllowHTTPMethods: []string{"post"},
			Params: nil})

	beego.GlobalControllerRouter["gitlab.com/edot92/gogateway/controllers:TagController"] = append(beego.GlobalControllerRouter["gitlab.com/edot92/gogateway/controllers:TagController"],
		beego.ControllerComments{
			Method: "Deletetabletags",
			Router: `/deletetabletags`,
			AllowHTTPMethods: []string{"post"},
			Params: nil})

	beego.GlobalControllerRouter["gitlab.com/edot92/gogateway/controllers:TcepekController"] = append(beego.GlobalControllerRouter["gitlab.com/edot92/gogateway/controllers:TcepekController"],
		beego.ControllerComments{
			Method: "Getmodbustcpsetting",
			Router: `/getmodbustcpsetting`,
			AllowHTTPMethods: []string{"get"},
			Params: nil})

	beego.GlobalControllerRouter["gitlab.com/edot92/gogateway/controllers:UsbController"] = append(beego.GlobalControllerRouter["gitlab.com/edot92/gogateway/controllers:UsbController"],
		beego.ControllerComments{
			Method: "Getportavailable",
			Router: `/getportavailable`,
			AllowHTTPMethods: []string{"get"},
			Params: nil})

	beego.GlobalControllerRouter["gitlab.com/edot92/gogateway/controllers:UsbController"] = append(beego.GlobalControllerRouter["gitlab.com/edot92/gogateway/controllers:UsbController"],
		beego.ControllerComments{
			Method: "Getportalias",
			Router: `/getportalias`,
			AllowHTTPMethods: []string{"get"},
			Params: nil})

	beego.GlobalControllerRouter["gitlab.com/edot92/gogateway/controllers:UsbController"] = append(beego.GlobalControllerRouter["gitlab.com/edot92/gogateway/controllers:UsbController"],
		beego.ControllerComments{
			Method: "Updateportalias",
			Router: `/updateportalias`,
			AllowHTTPMethods: []string{"post"},
			Params: nil})

}
