package main

import (
	"fmt"
	"log"
	"time"

	"github.com/astaxie/beego"
	"github.com/jasonlvhit/gocron"
	koneksi "gitlab.com/edot92/gogateway/koneksi"
	"gitlab.com/edot92/gogateway/models"
)

func UpdateToOtherDB() {
	fmt.Println("start update")
	enableUpdateOther, err := beego.AppConfig.Bool("ENABLE_OTHERDB")
	if enableUpdateOther == false && err != nil {
		log.Println(err.Error() + "atau nilai ENABLE_OTHERDB di conf/otherdb.conf=false ")
		fmt.Println("error load")

		return
	}
	_, err = koneksi.CobaKonekOtherDB()
	if err != nil {
		strErr := "error konek ke other db:" + err.Error()
		fmt.Println(strErr)

		log.Println(strErr)
		return
	}
	defer koneksi.ConOtherdb.Close()
	res, err := models.GetListTagsFromSelfDB()
	if err != nil {
		strErr := "error GetListTagsFromSelfDB:" + err.Error()
		fmt.Println(strErr)

		log.Println(strErr)
		return
	}
	err = models.UpdateToOtherDb(res)
	if err != nil {
		strErr := "error UpdateToOtherDb:" + err.Error()
		fmt.Println(strErr)
		log.Println(strErr)
		return
	}

}

func ScheduleOthetrDB() {
	time.Sleep(5000)
	jedaUpdate, _ := beego.AppConfig.Int("JEDA_UPDATE")
	for {
		gocron.Every(uint64(jedaUpdate)).Seconds().Do(UpdateToOtherDB)
		<-gocron.Start()
	}
}
