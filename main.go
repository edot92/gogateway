package main

import (
	"encoding/binary"
	"errors"
	"fmt"
	"math"
	"os"
	"strconv"
	"strings"
	"sync"
	"time"

	beegoFrontend "github.com/astaxie/beego"
	"github.com/fatedier/frp/src/models/client"
	"github.com/fatedier/frp/src/utils/log"
	modbustcp "gitlab.com/edot92/gogateway/engine/modbustcp"

	modbusdb "gitlab.com/edot92/gogateway/engine/modbusdb"
	portserialdb "gitlab.com/edot92/gogateway/engine/portserialdb"

	koneksi "gitlab.com/edot92/gogateway/koneksi"

	_ "gitlab.com/edot92/gogateway/routers"

	_ "github.com/go-sql-driver/mysql"
	"github.com/goburrow/modbus"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
	serial "go.bug.st/serial.v1"
)

var (
	configFile string = "./frpc.ini"
)

var usage string = `frpc is the client of frp
Usage:
    frpc [-c config_file] [-L log_file] [--log-level=<log_level>] [--server-addr=<server_addr>]
    frpc -h | --help
    frpc -v | --version
Options:
    -c config_file              set config file
    -L log_file                 set output log file, including console
    --log-level=<log_level>     set log level: debug, info, warn, error
    --server-addr=<server_addr> addr which frps is listening for, example: 0.0.0.0:7000
    -h --help                   show this screen
    --version                   show version
`

type modbusVariabelStruct struct {
	alamat   uint16
	value    uint16
	active   bool
	rw       string
	typedata string
}

// ModbusVariabelRTU ...
var ModbusVariabelRTU [65535]modbusVariabelStruct

// ModbusVariabelTCP ...
var ModbusVariabelTCP [65535]modbusVariabelStruct

//KonDb global koneksi
var KonDb *gorm.DB
var h *handler
var registerHolding []uint16

//DataType list data type modbus
var DataType = map[string]uint8{"INT16BE": 1, "UINT16BE": 1, "INT16LE": 1, "UINT16LE": 1, "INT32BE": 2, "UINT32BE": 2, "INT32LE": 2, "UINT32LE": 2, "FLOAT32LE": 4, "FLOAT64LE": 4, "FLOAT32BE": 8, "FLOAT64BE": 8}
var startRegister uint16 = 40000

type handler struct {
	*modbustcp.MbTcp
}

func (h *handler) MtcpReadHoldingRegister(startAddr uint16, qtyAddr uint16) ([]uint16, uint8) {
	count := int(qtyAddr)
	res := make([]uint16, qtyAddr)
	for index := 0; index < count; index++ {
		res[index] = uint16(ModbusVariabelTCP[startAddr+uint16(index)].value)
	}
	fmt.Println("byte Holding:", res)
	return res, 0
}
func (h *handler) MtcpPresetMultipleRegisters(startAddr uint16, qtyAddr uint16, valueToWrite []uint16) ([]uint16, uint8) {
	fmt.Println("start addr:", startAddr)
	fmt.Println("qty addr:", qtyAddr)
	count := int(qtyAddr)
	res := make([]uint16, qtyAddr)
	for index := 0; index < count; index++ {
		ModbusVariabelRTU[startAddr+uint16(index)].value = valueToWrite[index]
	}
	fmt.Println("register write", registerHolding[startAddr:(startAddr+uint16(count))])

	return res, 0
}
func (h *handler) MtcpPresetSingleRegister(startAddr uint16, valueToWrite uint16) uint8 {
	fmt.Println("start addr:", startAddr)
	fmt.Println("value addr:", valueToWrite)
	registerHolding[startAddr] = valueToWrite
	fmt.Println("register write", registerHolding[startAddr])
	ModbusVariabelRTU[startAddr].value = valueToWrite
	go func() {
		modbusdb.UpdateValueFromTcpWrite(startAddr+startRegister, strconv.FormatUint(uint64(valueToWrite), 10))
	}()
	return 0 //error value
}
func (h *handler) Server(req []byte) []byte {
	return []byte{}
}

func (h *handler) Fault(detail string) {

}

// doTCPModbusServer run port modbus tcp
func doTCPModbusServer() {

	modbustcp.SetHandler(h)
	fmt.Println("modbus tcp on PORT:", koneksi.WebApi.Port)
	fmt.Println("modbus tcp on ADDDR:", koneksi.WebApi.Address)
	go func() {
		for {
			// if koneksi.WebApi.Status == true {
			modbustcp.ServerCreate(koneksi.WebApi.Port, koneksi.WebApi.Address)
		}
		// }
	}()
	// fmt.Fatal("tes")

}

func scantoDbUSBActive() bool {
	ports, err := serial.GetPortsList()
	if err != nil {
		// fmt.Println(err)
	}
	if len(ports) == 0 {
		fmt.Println("No serial ports found!")
		// ksongkan serial port yang terdaftar
		koneksi.Condb.Exec("DELETE from usbtersedias")
		return false
	}
	portserialdb.SavetoDbUSBActive(ports)
	return true

}

// RtuModbusClient fungsi buka port
func RtuModbusClient(portActive koneksi.TagListActive) (modbus.Client, *modbus.RTUClientHandler, error) {
	var comPort, temPort = portActive.UsbSystem, ""
	ports, err := serial.GetPortsList()

	if err != nil {
		fmt.Println(err)
		return nil, nil, err
	}
	if len(ports) == 0 {
		// fmt.Println("No serial ports found!")
		return nil, nil, errors.New("no serial ports found")
	}
	for _, port := range ports {
		// fmt.Printf("PORT TERSEDIA: %v\n", port)

		if port == comPort {
			temPort = port
		}
	}

	if temPort == "" {
		return nil, nil, errors.New("PORT:" + comPort + " YANG DI PILIH TIDAK TERSEDIA")
	}
	parity := ""
	switch strings.ToLower(portActive.IdparityController) {
	case "none":
		parity = "N"
	case "even":
		parity = "E"
	case "odd":
		parity = "O"
	}
	slaveID := portActive.IdstationController
	handlerRtu := modbus.NewRTUClientHandler(temPort)
	handlerRtu.BaudRate, _ = strconv.Atoi(portActive.IdbaudController)
	handlerRtu.DataBits, _ = strconv.Atoi(portActive.IddatabitController)
	handlerRtu.Parity = parity
	handlerRtu.StopBits, _ = strconv.Atoi(portActive.IdstopbitsController)
	handlerRtu.SlaveId = byte(slaveID)
	handlerRtu.Timeout = 5 * time.Second

	err = handlerRtu.Connect()

	if err != nil {
		return nil, nil, err
	}
	client := modbus.NewClient(handlerRtu)

	return client, handlerRtu, nil
}

//
func parseBitModbus(res []byte, dataTypeStr string) string {
	var newValue string
	switch dataTypeStr {
	case "UINT16LE":
		newValue = strconv.FormatUint(uint64(binary.LittleEndian.Uint16(res)), 10)
	case "UINT16BE":
		newValue = strconv.FormatUint(uint64(binary.BigEndian.Uint16(res)), 10)
	case "INT16LE":
		newValue = strconv.FormatInt(int64(binary.LittleEndian.Uint16(res)), 10)
	case "INT16BE":
		newValue = strconv.FormatInt(int64(binary.BigEndian.Uint16(res)), 10)
	case "UINT32LE":
		newValue = strconv.FormatUint(uint64(binary.LittleEndian.Uint32(res)), 10)
	case "UINT32BE":
		newValue = strconv.FormatUint(uint64(binary.BigEndian.Uint32(res)), 10)
	case "INT32LE":
		newValue = strconv.FormatInt(int64(binary.LittleEndian.Uint32(res)), 10)
	case "INT32BE":
		newValue = strconv.FormatInt(int64(binary.BigEndian.Uint32(res)), 10)
	case "FLOAT32LE":
		{
			bits := binary.LittleEndian.Uint32(res)
			float := float64(math.Float32frombits(bits))
			newValue = strconv.FormatFloat(float, 'f', 2, 32)
		}
	case "FLOAT32BE":
		{
			bits := binary.BigEndian.Uint32(res)
			float := float64(math.Float32frombits(bits))
			newValue = strconv.FormatFloat(float, 'f', 2, 32)
		}
	case "FLOAT64LE":
		{
			bits := binary.LittleEndian.Uint64(res)
			float := (math.Float64frombits(bits))
			newValue = strconv.FormatFloat(float, 'f', 4, 64)
		}
	case "FLOAT64BE":
		{
			bits := binary.BigEndian.Uint64(res)
			float := float64(math.Float64frombits(bits))
			newValue = strconv.FormatFloat(float, 'f', 4, 64)
		}
	default:
		{
			fmt.Println("task7")

			fmt.Println("data type tidak sesuai")

		}
	} //end switch case
	return newValue
}

func movRegisterToTCP(startAddr uint16, res []byte, dataTypeStr string) {
	switch dataTypeStr {
	case "UINT16LE":
		a := res[0:2]
		ModbusVariabelTCP[startAddr].value = uint16(uint64(binary.BigEndian.Uint16(a)))
	case "UINT16BE":
		a := res[0:2]
		ModbusVariabelTCP[startAddr].value = uint16(uint64(binary.BigEndian.Uint16(a)))
	case "INT16LE":
		a := res[0:2]
		ModbusVariabelTCP[startAddr].value = uint16(uint64(binary.BigEndian.Uint16(a)))
	case "INT16BE":
		a := res[0:2]
		ModbusVariabelTCP[startAddr].value = uint16(uint64(binary.BigEndian.Uint16(a)))
	case "UINT32LE":
		{
			a := res[0:2]
			b := res[2:4]
			ModbusVariabelTCP[startAddr].value = uint16(uint64(binary.BigEndian.Uint16(a)))
			ModbusVariabelTCP[startAddr+1].value = uint16(uint64(binary.BigEndian.Uint16(b)))
		}
	case "UINT32BE":
		{
			a := res[0:2]
			b := res[2:4]
			ModbusVariabelTCP[startAddr].value = uint16(uint64(binary.BigEndian.Uint16(a)))
			ModbusVariabelTCP[startAddr+1].value = uint16(uint64(binary.BigEndian.Uint16(b)))
		}
	case "INT32LE":
		{
			a := res[0:2]
			b := res[2:4]
			ModbusVariabelTCP[startAddr].value = uint16(uint64(binary.BigEndian.Uint16(a)))
			ModbusVariabelTCP[startAddr+1].value = uint16(uint64(binary.BigEndian.Uint16(b)))
		}
	case "INT32BE":
		{
			a := res[0:2]
			b := res[2:4]
			ModbusVariabelTCP[startAddr].value = uint16(uint64(binary.BigEndian.Uint16(a)))
			ModbusVariabelTCP[startAddr+1].value = uint16(uint64(binary.BigEndian.Uint16(b)))

		}
	case "FLOAT32LE":
		{
			a := res[0:2]
			b := res[2:4]
			ModbusVariabelTCP[startAddr].value = uint16(uint64(binary.BigEndian.Uint16(a)))
			ModbusVariabelTCP[startAddr+1].value = uint16(uint64(binary.BigEndian.Uint16(b)))
		}
	case "FLOAT32BE":
		{
			a := res[0:2]
			b := res[2:4]
			ModbusVariabelTCP[startAddr].value = uint16(uint64(binary.BigEndian.Uint16(a)))
			ModbusVariabelTCP[startAddr+1].value = uint16(uint64(binary.BigEndian.Uint16(b)))
		}
	case "FLOAT64LE":
		{
			a := res[0:2]
			b := res[2:4]
			c := res[4:6]
			d := res[6:8]
			ModbusVariabelTCP[startAddr].value = uint16(uint64(binary.BigEndian.Uint16(a)))
			ModbusVariabelTCP[startAddr+1].value = uint16(uint64(binary.BigEndian.Uint16(b)))
			ModbusVariabelTCP[startAddr+2].value = uint16(uint64(binary.BigEndian.Uint16(c)))
			ModbusVariabelTCP[startAddr+3].value = uint16(uint64(binary.BigEndian.Uint16(d)))

		}
	case "FLOAT64BE":
		{
			a := res[0:2]
			b := res[2:4]
			c := res[4:6]
			d := res[6:8]
			ModbusVariabelTCP[startAddr].value = uint16(uint64(binary.BigEndian.Uint16(a)))
			ModbusVariabelTCP[startAddr+1].value = uint16(uint64(binary.BigEndian.Uint16(b)))
			ModbusVariabelTCP[startAddr+2].value = uint16(uint64(binary.BigEndian.Uint16(c)))
			ModbusVariabelTCP[startAddr+3].value = uint16(uint64(binary.BigEndian.Uint16(d)))
		}
	default:
		{
			fmt.Println("task7")

			fmt.Println("data type tidak sesuai")

		}
	} //end switch case
}
func doWriteToSerialRTUModbus(res koneksi.TagListActive) error {
	var addr uint16
	addrr, err := strconv.Atoi(res.IdregisterfromcontrollerTag)
	if err == nil {
		addr = uint16(addrr) - startRegister
		// _ = addrr
	} else {
		return err
	}
	client, handlerRtuTemp, err := RtuModbusClient(res)

	var errnya string
	if err != nil {
		// errnya = err.Error()
		return err
	}
	defer handlerRtuTemp.Close()                                             // close koneksi serial
	temp, _ := strconv.Atoi(res.IdlastvalueTag)                              //konvert string ke uint16
	ModbusVariabelRTU[addr].value = uint16(temp)                             //pindahkan dari register database ke vatiabel global
	_, err = client.WriteSingleRegister(addr, ModbusVariabelRTU[addr].value) // kirim ke serial modbus
	if err != nil {
		// errnya = err.Error()
		errnya = err.Error()

		if errnya == "Could not enumerate serial ports" {
			// fmt.Println("tidak ada serial port")
			// handlerRtuTemp.Close()
			return errors.New("tidak ada serial port")
		}
	}
	return nil
}

// scanRWRegisters untuk loop modbus scan yang di dapat dari database
func scanRWRegisters(res []koneksi.TagListActive) {
	for index := 0; index < len(res); index++ {
		role := res[index].IdaccessrightTag

		var addr, qtyByteDataType uint16
		var dataTypeStr string
		idTabletaglist := res[index].ID
		fmt.Println("mode:", role)
		if role == "READ" {
			if res[index].IddatatypeTag != "" {
				if _, ok := DataType[res[index].IddatatypeTag]; ok {
					// do something
					qtyByteDataType = uint16(DataType[res[index].IddatatypeTag])
					dataTypeStr = res[index].IddatatypeTag
				}
			}
			// konersi string database ke integer variabel
			if addrr, err := strconv.Atoi(res[index].IdregisterfromcontrollerTag); err != nil {
				fmt.Println("error converting")
				continue
			} else {
				// pindah varibel int ke variabel addr dengan di kurang start register yang telah di tetapkan
				addr = uint16(addrr) - startRegister
				_ = addrr
			}

			// var handlerRtuTemp *modbus.RTUClientHandler
			client, handlerRtuTemp, err := RtuModbusClient(res[index])
			var errnya string
			if err != nil {
				fmt.Println("Error:", err)
				errnya = err.Error()
			}
			if errnya == "Could not enumerate serial ports" {
				fmt.Println("tidak ada serial port")

				continue
			} else if errnya != "" {
				fmt.Println("Error :", errnya)
				continue

			}
			client = modbus.NewClient(handlerRtuTemp)
			resSerialRtu, err := client.ReadHoldingRegisters(addr, qtyByteDataType)
			_ = qtyByteDataType
			if err != nil {
				strErr := "Error: " + err.Error() + ", when " + role + ", Register :" + res[index].IdregisterfromcontrollerTag + ",PORT:" + res[index].UsbSystem + ",Slave ID:" + strconv.Itoa(int(res[index].IdstationController)) + ",Baud:" + res[index].IdbaudController + ",DataBits:" + res[index].IddatabitController + ",StopBits:" + res[index].IdstopbitsController + ",Parity:" + res[index].IdparityController
				fmt.Println(strErr)
				checkError(err)
				handlerRtuTemp.Close()
				continue
			}
			newValue := parseBitModbus(resSerialRtu, dataTypeStr)
			/*************simpan ke internal ram ***********/
			// rtunya
			ModbusVariabelRTU[addr].value = binary.BigEndian.Uint16(resSerialRtu)
			// tcpnya
			// konersi string database ke integer variabel untuk modbus tcp
			if addrr, err := strconv.Atoi(res[index].IdregistertocontrollerTag); err != nil {
				fmt.Println("error converting")
				continue
			} else {

				// pindah varibel int ke variabel addr dengan di kurang start register yang telah di tetapkan
				addr = uint16(addrr) - startRegister
				// strTEmp := "MOVE TO TCP :" + addr, ",with val:" + string(binary.BigEndian.Uint16(resSerialRtu))
				// fmt.Println(strTEmp)
				movRegisterToTCP(addr, resSerialRtu, dataTypeStr)

				_ = addrr
			}

			if newValue == "" {
				fmt.Println("task8")
				fmt.Println("newValue kosong, data type tidak sesuai")
				handlerRtuTemp.Close()
				continue
			}
			/*************update nilai read modbus  ke database***********/
			// ambil nilai modbus dari perangkat
			if err := modbusdb.UpdateValueFromRtuRead(idTabletaglist, newValue); err != nil {
				fmt.Println("task9")
				checkError(err)
				handlerRtuTemp.Close()
				continue
			}
			fmt.Println("Read Reqister:", addr)
			fmt.Println("Val Reqister:", newValue)

			handlerRtuTemp.Close()
			_ = newValue

		} else if role == "WRITE" {
			doWriteToSerialRTUModbus(res[index])

		} else if role == "READWRITE" {

		} else {
			fmt.Println("error role not r,w or rw")
			continue
		}

	} //end scan
}

/*TaskModbusRtu TaskSchedule
1. scan gateway yang aktif dari database
2. Ambil value read
3. ambil nilai dari serial port
4. update ke database untuk data yang sudah terbaca dan simpan ke variabel internal untuk modbus TCP
5. ambil nilai value write
6. tulis nilai ke serial port dan simpan ke variabel internal
*/
func TaskModbusRtu() {
	var countTimer uint16
	countTimer = 0

	go func() {
		for {
			var lanjut = true
			fmt.Println("BEGIN MODBUS")
			if countTimer > 10 {
				lanjut = scantoDbUSBActive()
				countTimer = 0
			}
			countTimer++
			if lanjut == true {
				res, err := modbusdb.GetListActive()
				if err != nil {
					fmt.Println(err)
					continue
				}
				if len(res) == 0 {
					fmt.Println("tidak ada register")
					// time.Sleep(2000)
					continue
				}
				// koneksi.TagListActive
				scanRWRegisters(res)
			}
			// _ = res
			_ = lanjut
		}
	}()
}

func checkError(err error) {
	if err != nil {
		fmt.Println(err)
		// fmt.Fprintf(os.Stderr, "Fatal error: %s", err.Error())
		// os.Exit(1)
	}
}

// runWeb kode web run
func runWeb() {
	go func() {
		fmt.Println("Run Mode: ", beegoFrontend.BConfig.RunMode)
		beegoFrontend.SetStaticPath("/static", "static")
		if beegoFrontend.BConfig.RunMode == "dev" {
			beegoFrontend.BConfig.WebConfig.DirectoryIndex = true
			beegoFrontend.SetStaticPath("/webapi", "swagger")
		}
		beegoFrontend.Run()
	}()
}

func runTunnel() {
	err := client.LoadConf(configFile)
	if err != nil {
		fmt.Println(err)
		os.Exit(-1)
	}

	log.InitLog(client.LogWay, client.LogFile, client.LogLevel, client.LogMaxDays)

	// wait until all control goroutine exit
	var wait sync.WaitGroup
	wait.Add(len(client.ProxyClients))

	for _, client := range client.ProxyClients {
		go ControlProcess(client, &wait)
	}

	log.Info("Start frpc success")

	wait.Wait()
	log.Warn("All proxy exit!")
}
func main() {
	//init dummy variabel
	registerHolding = make([]uint16, 65536)
	for index := 0; index < 65535; index++ {
		registerHolding[index] = 0
		ModbusVariabelRTU[index].alamat = uint16(index)
		ModbusVariabelRTU[index].value = 0
		ModbusVariabelRTU[index].active = false
		ModbusVariabelRTU[index].rw = "READ"
		ModbusVariabelRTU[index].typedata = "INT16"
		ModbusVariabelTCP[index].alamat = uint16(index)
		ModbusVariabelTCP[index].value = 0
		ModbusVariabelTCP[index].active = false
		ModbusVariabelTCP[index].rw = "READ"
		ModbusVariabelTCP[index].typedata = "INT16"

	}
	var err error
	KonDb, err = koneksi.CobaKonek()
	if err != nil {
		checkError(err)
	} else {
		defer KonDb.Close()
	}
	runWeb()
	doTCPModbusServer()
	TaskModbusRtu()
	go ScheduleOthetrDB()
	// runTunnel()
	// time.Sleep(0)
	for {
		time.Sleep(100)
	}

}
