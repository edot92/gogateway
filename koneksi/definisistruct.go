package koneksi

import "time"

// RenderdataHTML ...
type RenderdataHTML struct {
	BaseURL      string
	ListBaud     []string
	ListPort     []string
	ListParity   []string
	ListDatabits []string
	ListStopbits []string
	// Website      string
	Email         string
	Handphone     string
	PORTModbusTCP string
}

// TagListActive ...
type TagListActive struct {
	ID                          uint
	IdnameTag                   string `gorm:"not null;unique"`
	IddatatypeTag               string
	IdnamefromcontrollerTag     string
	IdregisterfromcontrollerTag string
	IdnametocontrollerTag       string `gorm:"default:'SELF_CONTROLLER'"`
	IdregistertocontrollerTag   string `gorm:"unique"`
	IdlastvalueTag              string
	IdupdatetimeTag             time.Time
	IdactiveController          bool
	SelisihUpdateMultipleDb     string
	MultipleDb                  bool
	IdaccessrightTag            string
	StatusMultipleDb            bool
	UpdateMultipleDb            time.Time
	UpdatedAt                   time.Time
	CreatedAt                   time.Time
	PortProperties              ControllerList

	IdcommunicationmodeController string
	IdprotocolController          string
	IdstationController           uint
	IdportController              string
	IdbaudController              string
	IdparityController            string
	IddatabitController           string
	IdstopbitsController          string
	// dari field table Usbtersedia
	UsbSystem string
}

//TagList  Table ...
type TagList struct {
	ID                          uint   `gorm:"AUTO_INCREMENT;primary_key:yes"`
	IdnameTag                   string `gorm:"not null;unique"`
	IddatatypeTag               string `gorm:"default:'UINT16BE'"`
	IdnamefromcontrollerTag     string
	IdregisterfromcontrollerTag string
	IdnametocontrollerTag       string `gorm:"default:'SELF_CONTROLLER'"`
	IdregistertocontrollerTag   string `gorm:"default:'0'"`
	IdlastvalueTag              string `gorm:"default:'0'"`
	IdupdatetimeTag             time.Time
	IdactiveController          bool `gorm:"default:false"`
	SelisihUpdateMultipleDb     string
	MultipleDb                  bool
	IdaccessrightTag            string `gorm:"default:'READ'"`
	StatusMultipleDb            bool   `gorm:"default:false"`
	UpdateMultipleDb            time.Time
	UpdatedAt                   time.Time
	CreatedAt                   time.Time
}

// ControllerList ...
type ControllerList struct {
	ID                            uint   `gorm:"AUTO_INCREMENT;primary_key:yes"`
	IdnameController              string `gorm:"not null;unique"`
	IdactiveController            bool   `gorm:"default:false"`
	IdcommunicationmodeController string
	IdprotocolController          string
	IdstationController           int    `gorm:"not null;default:1"`
	IdportController              string `gorm:"not null;default:'PORT1'"`
	IdbaudController              string `gorm:"not null;default:'9600'"`
	IdparityController            string `gorm:"not null;default:'None'"`
	IddatabitController           string `gorm:"not null;default:'8'"`
	IdstopbitsController          string `gorm:"not null;default:'1'"`
	CreatedAt                     time.Time
	UpdatedAt                     time.Time
}

// Usbtersedias ...
type Usbtersedias struct {
	ID        int `gorm:"AUTO_INCREMENT;primary_key:yes"`
	UsbSystem string
	CreatedAt time.Time
	UpdatedAt time.Time
}

// Usbport ...
type Usbport struct {
	ID        int `gorm:"AUTO_INCREMENT;primary_key:yes"`
	UsbSystem string
	UsbAlias  string
	Status    bool
	UpdatedAt time.Time
	CreatedAt time.Time
}

//
type OptionWebApi struct {
	ID      int  `gorm:"AUTO_INCREMENT;primary_key:yes"`
	Status  bool `gorm:"not null;default:0"`
	Port    int  `gorm:"not null;default:1111"`
	Address int  `gorm:"not null;default:1"`
}
type webApi struct {
	Status  bool
	Port    int
	Address int
}
