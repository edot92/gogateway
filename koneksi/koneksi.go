// package koneksi

// import (
// 	"fmt"
// 	"log"

// 	lumberjack "gopkg.in/natefinch/lumberjack.v2"

// 	"strconv"
// 	"time"

// 	"github.com/astaxie/beego"
// 	"github.com/jinzhu/gorm"
// )

// // WebApi ...
// var WebApi webApi

// //PORTWeb ...
// var PORTWeb int

// // PORTApi ....
// var PORTApi int

// // PORTModbusTCP ....
// var PORTModbusTCP int

// // BeegoRunMode ....
// var BeegoRunMode string

// // Db global koneksi
// var (
// 	// this one gonna contain an open connection
// 	// make sure to call Connect() before using it
// 	Condb *gorm.DB

// 	// Env *config.Config
// )

// // TypeDatabase ...
// var TypeDatabase string

// //
// // var LogError lumberjack

// func init() {
// 	// LogError := &lumberjack.Logger{
// 	// 	Filename:   "/var/log/myapp/foo.log",
// 	// 	MaxSize:    500, // megabytes
// 	// 	MaxBackups: 3,
// 	// 	MaxAge:     28, // days
// 	// }
// 	// LogError.Write([]byte("tes"))
// 	// LogError := &lumberjack.Logger{
// 	// 	Filename:   "error_.log",
// 	// 	MaxSize:    1000, // megabytes
// 	// 	MaxBackups: 100,
// 	// 	MaxAge:     30, //days
// 	// }
// 	log.SetOutput(&lumberjack.Logger{
// 		Filename:   "error.log",
// 		MaxSize:    10, // megabytes
// 		MaxBackups: 100,
// 		MaxAge:     1, //days
// 	})
// }

// func getTimeNow() time.Time {
// 	now := time.Now()
// 	return (now)
// }

// // CobaKonek ke database
// func CobaKonek() (*gorm.DB, error) {
// 	// Env, errRead := config.ReadDefault("pengaturan.env")
// 	var db *gorm.DB
// 	var err error
// 	beego.AppConfig.String("TYPE_DATABASE")
// 	typeDatabase := beego.AppConfig.String("TYPE_DATABASE")
// 	if typeDatabase == "mysql" {
// 		user := beego.AppConfig.String("DB_USERNAME")
// 		pass := beego.AppConfig.String("DB_PASSWORD")
// 		database := beego.AppConfig.String("DB_DATABASE")
// 		host := beego.AppConfig.String("DB_HOST")
// 		port := beego.AppConfig.String("DB_PORT")
// 		db, err = gorm.Open("mysql", user+":"+pass+"@tcp("+host+":"+port+")/"+database+"?charset=utf8&parseTime=True&loc=Local&writeTimeout=5s&readTimeout=5s&timeout=5s")
// 	} else if typeDatabase == "sqlite3" {
// 		database := beego.AppConfig.String("DB_DATABASE")
// 		db, err = gorm.Open("sqlite3", database+".db")
// 	}
// 	// db, err := gorm.Open("mysql", "root:@/gatewaydb")
// 	if err != nil {
// 		// log.Fatal(err)
// 		panic(err)

// 	} else {
// 		log.Println("terkoneksi dengan database")

// 	}
// 	db.LogMode(false)

// 	// db.DB().SetConnMaxLifetime(0)
// 	// db.DB().SetMaxIdleConns(100)
// 	// db.DB().SetMaxOpenConns(5)
// 	// db.DB().SetMaxIdleConns(10)
// 	// db.DB().SetMaxOpenConns(100)

// 	// db.DB().SetMaxIdleConns(0)
// 	// db.DB().SetMaxOpenConns(0)
// 	if db.HasTable("tag_lists") == false {
// 		fmt.Println("Created table tag_lists")
// 		db.AutoMigrate(&TagList{})
// 	}
// 	if db.HasTable("controller_lists") == false {
// 		fmt.Println("Created table controller_lists")
// 		db.AutoMigrate(&ControllerList{})
// 	}
// 	if db.HasTable("usbports") == false {
// 		fmt.Println("Created table usbports")
// 		db.AutoMigrate(&Usbport{})
// 		for i := 0; i < 4; i++ {
// 			var usbPort = Usbport{UsbAlias: "PORT" + strconv.Itoa(i+1), Status: false, CreatedAt: time.Now(), UpdatedAt: time.Now()}
// 			db.Create(&usbPort)
// 		}
// 	}
// 	if db.HasTable("usbtersedias") == false {
// 		fmt.Println("Created table usbtersedias")
// 		db.AutoMigrate(&Usbtersedias{})

// 	}

// 	// if db.HasTable("option_web_api") == false {
// 	// 	db.AutoMigrate(&OptionWebApi{})
// 	// 	fmt.Println("Created table OptionWebApi")
// 	// 	var data = OptionWebApi{ID: 1}
// 	// 	db.Create(&data)
// 	// 	WebApi.Address = 1
// 	// 	WebApi.Port = 1111
// 	// 	WebApi.Status = false
// 	// } else {
// 	// 	var data OptionWebApi
// 	// 	db.First(&data)
// 	// 	WebApi.Address = data.Address
// 	// 	WebApi.Port = data.Port
// 	// 	WebApi.Status = data.Status
// 	// }

// 	WebApi.Port, err = beego.AppConfig.Int("PORT_MODBUS_TCP_PORT")
// 	WebApi.Address, _ = beego.AppConfig.Int("PORT_MODBUS_TCP_ADDR")
// 	if err != nil {
// 		fmt.Println(err)
// 		WebApi.Port = 502
// 			WebApi.Address = 1
// 	}
// 	WebApi.Status = true
// 	Condb = db
// 	TypeDatabase = typeDatabase
// 	// log.Fatal("exit")
// 	return db, err
// }

package koneksi

import (
	"fmt"
	"log"

	lumberjack "gopkg.in/natefinch/lumberjack.v2"

	"strconv"
	"time"

	"github.com/astaxie/beego"
	"github.com/jinzhu/gorm"
)

var LogBee lumberjack.Logger

// WebApi ...
var WebApi webApi

//PORTWeb ...
var PORTWeb int

// PORTApi ....
var PORTApi int

// PORTModbusTCP ....
var PORTModbusTCP int

// BeegoRunMode ....
var BeegoRunMode string

// Db global koneksi
var (
	// this one gonna contain an open connection
	// make sure to call Connect() before using it
	Condb      *gorm.DB
	Condb2     *gorm.DB
	ConOtherdb *gorm.DB
	// Env *config.Config
)

// TypeDatabase ...
var TypeDatabase string

//
// var LogError lumberjack
func init() {
	log.SetOutput(&lumberjack.Logger{
		Filename:   "log/info.log",
		MaxSize:    1, // megabytes
		MaxBackups: 10,
		MaxAge:     1, //days
	})
	// LogBee := logs.NewLogger()
	// LogBee.SetLogger(logs.AdapterFile, `{"filename":"log/info.log"}`)
	// LogBee.EnableFuncCallDepth(true)
	// LogBee.Async()
}

func getTimeNow() time.Time {
	now := time.Now()
	return (now)
}

// CobaKonek ke database
func CobaKonek() (*gorm.DB, error) {
	// Env, errRead := config.ReadDefault("pengaturan.env")
	var db *gorm.DB
	var err error
	beego.AppConfig.String("TYPE_DATABASE")
	typeDatabase := beego.AppConfig.String("TYPE_DATABASE")
	if typeDatabase == "mysql" {
		user := beego.AppConfig.String("DB_USERNAME")
		pass := beego.AppConfig.String("DB_PASSWORD")
		database := beego.AppConfig.String("DB_DATABASE")
		host := beego.AppConfig.String("DB_HOST")
		port := beego.AppConfig.String("DB_PORT")
		db, err = gorm.Open("mysql", user+":"+pass+"@tcp("+host+":"+port+")/"+database+"?charset=utf8&parseTime=True&loc=Local&writeTimeout=5s&readTimeout=5s&timeout=5s")
		db.DB().SetConnMaxLifetime(0)
		db.DB().SetMaxIdleConns(100)
		db.DB().SetMaxOpenConns(5)
		db.DB().SetMaxIdleConns(10)
		db.DB().SetMaxOpenConns(100)
	} else if typeDatabase == "sqlite3" {
		database := beego.AppConfig.String("DB_DATABASE")
		db, err = gorm.Open("sqlite3", database+".db")
	}
	// db, err := gorm.Open("mysql", "root:@/gatewaydb")
	if err != nil {
		panic(err)

	} else {
		log.Println("terkoneksi dengan database")
	}
	db.LogMode(false)

	if db.HasTable("tag_lists") == false {
		fmt.Println("Created table tag_lists")
		db.AutoMigrate(&TagList{})
	}
	if db.HasTable("controller_lists") == false {
		fmt.Println("Created table controller_lists")
		db.AutoMigrate(&ControllerList{})
	}
	if db.HasTable("usbports") == false {
		fmt.Println("Created table usbports")
		db.AutoMigrate(&Usbport{})
		for i := 0; i < 4; i++ {
			var usbPort = Usbport{UsbAlias: "PORT" + strconv.Itoa(i+1), Status: false, CreatedAt: time.Now(), UpdatedAt: time.Now()}
			db.Create(&usbPort)
		}
	}
	if db.HasTable("usbtersedias") == false {
		fmt.Println("Created table usbtersedias")
		db.AutoMigrate(&Usbtersedias{})

	}

	WebApi.Port, err = beego.AppConfig.Int("PORT_MODBUS_TCP_PORT")
	WebApi.Address, _ = beego.AppConfig.Int("PORT_MODBUS_TCP_ADDR")
	if err != nil {
		fmt.Println(err)
		WebApi.Port = 502
		WebApi.Address = 1
	}
	WebApi.Status = true
	Condb = db
	TypeDatabase = typeDatabase
	// cek apakah ada multiple DB

	enableUpdateOther, err := beego.AppConfig.Bool("ENABLE_OTHERDB")
	if enableUpdateOther == false && err != nil {
		log.Println(err.Error() + "atau nilai ENABLE_OTHERDB di conf/otherdb.conf=false ")

	} else {
		user := beego.AppConfig.String("DB_USERNAME")
		pass := beego.AppConfig.String("DB_PASSWORD")
		database := beego.AppConfig.String("DB_DATABASE")
		host := beego.AppConfig.String("DB_HOST")
		port := beego.AppConfig.String("DB_PORT")
		db2, err := gorm.Open("mysql", user+":"+pass+"@tcp("+host+":"+port+")/"+database+"?charset=utf8&parseTime=True&loc=Local&writeTimeout=5s&readTimeout=5s&timeout=5s")
		if err != nil {
			panic(err)

		}
		Condb2 = db2
	}

	return db, nil
}

func CobaKonekOtherDB() (*gorm.DB, error) {
	user := beego.AppConfig.String("DB_USERNAME_OTHERDB")
	pass := beego.AppConfig.String("DB_PASSWORD_OTHERDB")
	database := beego.AppConfig.String("DB_DATABASE_OTHERDB")
	host := beego.AppConfig.String("DB_HOST_OTHERDB")
	port := beego.AppConfig.String("DB_PORT_OTHERDB")
	dbOther, err := gorm.Open("mysql", user+":"+pass+"@tcp("+host+":"+port+")/"+database+"?charset=utf8&parseTime=True&loc=Local&writeTimeout=5s&readTimeout=5s&timeout=5s")
	if err != nil {
		log.Println("error koneksi other db ERR:" + err.Error())
		return nil, err
	}
	ConOtherdb = dbOther
	return dbOther, nil
}
