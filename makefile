export PATH := $(GOPATH)/bin:$(PATH)
export GO15VENDOREXPERIMENT := 1
LDFLAGS := -s -w

all: build

build: app

app:
	env CGO_ENABLED=1 GOOS=linux GOARCH=386 CC=/usr/bin/gcc  go build -ldflags "$(LDFLAGS)" -o ./client_linux_386 