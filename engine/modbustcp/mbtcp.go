package modbustcp

import (
	"fmt"
	"io/ioutil"
	"log"
	"net"
	"os"
	"strconv"
)

var _server func(req []byte) (res []byte)
var _readHoldingRegisters func(startAddr uint16, qtyAddr uint16) (res []uint16, errorCode uint8)
var _presetMultipleRegisters func(startAddr uint16, qtyAddr uint16, valueToWrite []uint16) (res []uint16, errorCode uint8)
var _presetSingleRegister func(startAddr uint16, valueToWrite uint16) (errorCode uint8)

var _fault func(detail string)
var debugYe = true

type Handler interface {
	Server(req []byte) (res []byte)
	MtcpReadHoldingRegister(startAddr uint16, qtyAddr uint16) (res []uint16, errorCode uint8)
	MtcpPresetMultipleRegisters(startAddr uint16, qtyAddr uint16, valueToWrite []uint16) (res []uint16, errorCode uint8)
	MtcpPresetSingleRegister(startAddr uint16, valueToWrite uint16) (errorCode uint8)
	Fault(detail string)
}

type MbTcp struct {
	Addr byte
	Code byte
	Data []byte
}

// type exceptionS struct {
// 	codeError map[int]string
// }

// var exception exceptionS
var alamatPerangkatIni int

func init() {

	// exception.codeError[1] = "ILLEGAL FUNCTION"
	// exception.codeError[2] = "ILLEGAL DATA ADDRESS"
	// exception.codeError[3] = "ILLEGAL DATA VALUE"
	// exception.codeError[4] = "SLAVE DEVICE FAILURE"
	// exception.codeError[5] = "ACKNOWLEDGE"
	// exception.codeError[6] = "SLAVE DEVICE BUSY"

}

func (m MbTcp) generate() []byte {
	head := make([]byte, 8, 8)
	l := byte(len(m.Data) + 2)
	head[0] = 0x00
	head[1] = 0x00
	head[2] = 0x00
	head[3] = 0x00
	head[4] = 0x00
	head[5] = l
	head[6] = m.Addr
	head[7] = m.Code
	body := make([]byte, 260)
	body = append(body, head...)
	body = append(body, m.Data...)
	return body
}

func (m *MbTcp) Send(addr string) ([]byte, error) {
	fmt.Print("(m *MbTcp) Send(addr string) ([]byte, error)")
	req := m.generate()
	return send(addr, req)
}

func send(a string, d []byte) ([]byte, error) {
	addr, err := net.ResolveTCPAddr("tcp4", a)
	if err == nil {
		c, err := net.DialTCP("tcp", nil, addr)
		if err == nil {
			_, err = c.Write(d)
			if err == nil {
				r, err := ioutil.ReadAll(c)
				if err == nil {
					return r, nil
				}
			}
		}
	}
	return []byte{}, err
}

func SetHandler(h Handler) {
	_server = h.Server
	_fault = h.Fault
	_readHoldingRegisters = h.MtcpReadHoldingRegister
	_presetMultipleRegisters = h.MtcpPresetMultipleRegisters
	_presetSingleRegister = h.MtcpPresetSingleRegister
}

func ServerCreate(port int, alamatnya int) error {
	log.Println(port)
	p := strconv.Itoa(port)
	ln, err := net.Listen("tcp", ":"+p)
	if err != nil {
		_fault(err.Error())
		return err
	}
	fmt.Println("create new serve")
	for {
		alamatPerangkatIni = alamatnya
		fmt.Println("start Listening port:", port)
		conn, err := ln.Accept()
		if err != nil {
			// handle error
			fmt.Print("error accept")
			continue
		}
		fmt.Println("accept connection from:", conn.RemoteAddr())
		go handleClient(conn) // go routine :) create new thread , non blocking koneksi
	}
}

func handleClient(conn net.Conn) {
	buf := make([]byte, 512)
	for {
		_, err := conn.Read(buf)
		if err != nil {
			conn.Close()
			return
		}
		if len(buf) >= 13 {
			if debugYe {
				// fmt.Println((buf))
				// fmt.Println(len(buf))
			}
			res := ParseDataTcp(buf)
			if debugYe {
				fmt.Print("exit")
			}
			if res == nil {
				continue
			}
			_, err := conn.Write(res)
			if err != nil {
				if debugYe {
					fmt.Print("error write")
				}
				continue
			} else {

			}
		}

	}
}

func ParseDataTcp(b []byte) []byte {

	// Packet indeks, x = 1 byte and xx= 2 byte and x... uknown size arraydepending of byte request
	ti := uint16(uint(b[1]) | uint((b[0]))>>8)   // xx: Transaction ID
	pi := uint16(uint(b[3]) | uint((b[2]))>>8)   // xx: Protocol ID
	ml := uint16(uint(b[5]) | uint((b[4]))>>8)   // xx: Message Length
	ui := uint16(b[6])                           // x:  Unit Identifier /ID  slave
	fc := uint16(b[7])                           // x:  Function Code
	ds := uint16(uint(b[9]) | uint((b[8]))>>8)   // xx:  Data Address start
	dq := uint16(uint(b[11]) | uint((b[10]))>>8) // xx:  data register qty
	if int(ui) != alamatPerangkatIni {
		return nil
	}
	var responseYuhuu []byte
	if fc == 3 { // holding register
		resTemp, _ := _readHoldingRegisters(ds, dq) // call function on main (interface) return array uint16
		//format response
		hiQty, LoQty := splitIntToByte1(dq*2 + 1)
		responseYuhuu = append(responseYuhuu, b[0], b[1])                 //xx: Transaction Identifier
		responseYuhuu = append(responseYuhuu, b[2], b[3])                 //xx: Protocol Identifier
		responseYuhuu = append(responseYuhuu, hiQty, LoQty)               //x: Message Length(Qty request *2) karena dihitung byte
		responseYuhuu = append(responseYuhuu, b[6])                       //x:  Unit Identifier /ID  slave
		responseYuhuu = append(responseYuhuu, b[7])                       //x: Function Code
		responseYuhuu = append(responseYuhuu, byte(dq*2))                 //x:
		responseYuhuu = append(responseYuhuu, intToByteArray(resTemp)...) //array pakai ... cause uknown lenght :)

		if debugYe {
			// fmt.Println("req type: Read Holding Register")
			// fmt.Println("req:", b)
			// fmt.Println("res:", responseYuhuu)
		}
		return responseYuhuu
	} else if fc == 6 { //Preset Single Register
		// fmt.Println(uint16(uint(b[10])))
		// fmt.Println(uint16(uint(b[11])))
		// fmt.Println(uint16((b[10]))<<8 | uint16((b[11])))
		// log.Fatal(b)
		valuePresetSingleRegister := uint16((b[10]))<<8 | uint16(b[11])
		err := _presetSingleRegister(ds, valuePresetSingleRegister)
		hiQty, LoQty := splitIntToByte1(dq*2 + 1)
		responseYuhuu = append(responseYuhuu, b[0], b[1])   //xx: Transaction Identifier
		responseYuhuu = append(responseYuhuu, b[2], b[3])   //xx: Protocol Identifier
		responseYuhuu = append(responseYuhuu, hiQty, LoQty) //x: Message Length(Qty request *2) karena dihitung byte
		responseYuhuu = append(responseYuhuu, b[6])         //x:  Unit Identifier /ID  slave
		responseYuhuu = append(responseYuhuu, b[7])         //x: Function Code
		responseYuhuu = append(responseYuhuu, b[8], b[9])   //xx: register addr
		responseYuhuu = append(responseYuhuu, b[10], b[11]) //xx: valueregister addr
		_ = err
		return responseYuhuu
	} else if fc == 16 { //_preset_Multiple_Registers
		bc := uint16(b[12])           // x:  data register qty ,bc = byte count -ammout of register
		var bw = make([]uint16, bc/2) // x... : byte write
		var i uint16
		for index := uint16(0); index < bc; index = index + 2 {
			bw[i] = uint16(uint(b[index+1+13]) | uint((b[index+13]))>>8)
			//fmt.Printf("indeks %d ,value %d", i, bw[i])
			i++
		}
		fmt.Println("qty reqister to write:", bc)
		resTemp, _ := _presetMultipleRegisters(ds, dq, bw) // call function on main (interface) return array uint16
		hiQty, LoQty := splitIntToByte1(dq*2 + 1)
		//format response
		responseYuhuu = append(responseYuhuu, b[0], b[1])   //xx: Transaction Identifier
		responseYuhuu = append(responseYuhuu, b[2], b[3])   //xx: Protocol Identifier
		responseYuhuu = append(responseYuhuu, hiQty, LoQty) //x: Message Length(Qty request *2) karena dihitung byte
		responseYuhuu = append(responseYuhuu, b[6])         //x:  Unit Identifier /ID  slave
		responseYuhuu = append(responseYuhuu, b[7])         //x: Function Code
		responseYuhuu = append(responseYuhuu, b[8], b[9])   //xx: //start register
		responseYuhuu = append(responseYuhuu, b[10], b[11]) //xx: qty register
		if debugYe {
			fmt.Println("req type: Preset Multiple Register")
		}
		_ = resTemp
		return responseYuhuu

	}
	// else {
	if debugYe {
		fmt.Println(fc)
		fmt.Printf("ti:%d ,pi:%d ,messagge length:%d, Unit Addr:%d, funcion code:%d ,data start:%d, qty:%d", ti,
			pi,
			ml,
			ui,
			fc,
			ds,
			dq)
	}
	responseYuhuu = append(responseYuhuu, b[0], b[1]) //xx: Transaction Identifier
	responseYuhuu = append(responseYuhuu, b[2], b[3]) //xx: Protocol Identifier
	responseYuhuu = append(responseYuhuu, 4, 0)       //x: Message Length(Qty *2) karena dihitung byte
	responseYuhuu = append(responseYuhuu, b[6])       //x:  Unit Identifier /ID  slave
	responseYuhuu = append(responseYuhuu, 0x80+b[7])  //x: Function Code
	responseYuhuu = append(responseYuhuu, 1)          //x: exception code ilegal function
	return responseYuhuu
	// }
}
func checkError(err error) {
	if err != nil {
		fmt.Fprintf(os.Stderr, "Fatal error: %s", err.Error())
		os.Exit(1)
	}
}

// func toWord(b []byte) byte {
// 	hi := uint(b[0])
// 	lo := uint(b[1])
// 	res := uint16(lo | (hi >> 8))
// 	return byte(res)
// }

func splitIntToByte1(i uint16) (Hi byte, Lo byte) {
	HiByte := (i >> 8) & 0xFF
	LoByte := i & 0xFF
	return byte(HiByte), byte(LoByte)
}

func splitIntToHILO(i uint16) []byte {
	res := make([]byte, 2)
	HiByte := (i >> 8) & 0xFF
	LoByte := i & 0xFF
	res[1] = byte(LoByte)
	res[0] = byte(HiByte)
	return res
}

func intToByteArray(d []uint16) []byte {
	var arr []byte
	if debugYe {
		fmt.Println("qty:", len(d))
	}
	for index := 0; index < len(d); index++ {
		tempArr := splitIntToHILO(d[index])
		// if debugYe {
		// 	fmt.Println("tempArr:", tempArr)
		// }
		arr = append(arr, tempArr...)
	}
	return arr
}
