package modbusaction

import (
	M "github.com/goburrow/modbus"
)

/*RtuWriteHoldingRegister fungsi mengambil register modbus*/
func RtuReadHoldingRegisters(client M.Client, addr uint16, qty uint16) ([]byte, error) {
	results, err := client.ReadHoldingRegisters(addr, qty)
	if err != nil {
		// panic(err)
		return nil, err
	}
	return results, nil
}
