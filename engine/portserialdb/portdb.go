package portserialdb

import (
	"time"

	koneksi "gitlab.com/edot92/gogateway/koneksi"
)

// GetPortActiveFromDb mendapatkanportyang aktif
// func GetPortActiveFromDb() string {

// }

// SavetoDbUSBActive ...
func SavetoDbUSBActive(portsActive []string) (bool, error) {
	koneksi.Condb.Exec("DELETE from usbtersedias")
	for i := 0; i < len(portsActive); i++ {
		var usbPort = koneksi.Usbtersedias{UsbSystem: portsActive[i], UpdatedAt: time.Now()}
		koneksi.Condb.Create(&usbPort)
	}
	return true, nil
}
