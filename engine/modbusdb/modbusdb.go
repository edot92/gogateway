package modbusdb

import (
	"errors"
	"fmt"
	"time"

	koneksi "gitlab.com/edot92/gogateway/koneksi"
)

// KoneksiGorm ....
// var KoneksiGorm *gorm.DB
// var cnt int

// // TagListActive ...
// type TagListActive struct {
// 	ID                          uint
// 	IdnameTag                   string
// 	IddatatypeTag               string
// 	IdnamefromcontrollerTag     string
// 	IdregisterfromcontrollerTag string
// 	IdnametocontrollerTag       string
// 	IdregistertocontrollerTag   string
// 	IdlastvalueTag              string
// 	IdupdatetimeTag             time.Time
// 	IdactiveController          bool
// 	SelisihUpdateMultipleDb     string
// 	MultipleDb                  bool
// 	IdaccessrightTag            string
// 	StatusMultipleDb            bool
// 	UpdateMultipleDb            time.Time
// 	UpdatedAt                   time.Time
// 	CreatedAt                   time.Time
// 	PortProperties              ControllerList

// 	IdcommunicationmodeController string
// 	IdprotocolController          string
// 	IdstationController           uint
// 	IdportController              string
// 	IdbaudController              string
// 	IdparityController            string
// 	IddatabitController           string
// 	IdstopbitsController          string
// 	// dari field table Usbtersedia
// 	UsbSystem string
// }

// //TagList  Table ...
// type TagList struct {
// 	ID                          uint
// 	IdnameTag                   string
// 	IddatatypeTag               string
// 	IdnamefromcontrollerTag     string
// 	IdregisterfromcontrollerTag string
// 	IdnametocontrollerTag       string
// 	IdregistertocontrollerTag   string
// 	IdlastvalueTag              string
// 	IdupdatetimeTag             time.Time
// 	IdactiveController          bool
// 	SelisihUpdateMultipleDb     string
// 	MultipleDb                  bool
// 	IdaccessrightTag            string
// 	StatusMultipleDb            bool
// 	UpdateMultipleDb            time.Time
// 	UpdatedAt                   time.Time
// 	CreatedAt                   time.Time
// }

// // ControllerList ...
// type ControllerList struct {
// 	ID                            uint
// 	IdnameController              string
// 	IdactiveController            bool
// 	IdcommunicationmodeController string
// 	IdprotocolController          string
// 	IdstationController           uint
// 	IdportController              string
// 	IdbaudController              string
// 	IdparityController            string
// 	IddatabitController           string
// 	IdstopbitsController          string
// 	CreatedAt                     time.Time
// 	UpdatedAt                     time.Time
// }

// // UsbTersedia ...
// type Usbtersedia struct {
// 	ID        int
// 	UsbSystem string
// 	CreatedAt time.Time
// 	UpdatedAt time.Time
// }

// // Usbport ...
// type Usbport struct {
// 	ID        int
// 	UsbSystem string
// }

// ReadAllTags ...
func ReadAllTags() ([]koneksi.TagList, error) {
	var res []koneksi.TagList
	koneksi.Condb.Find(&res)
	if len(res) > 0 {
		return res, nil
	}
	return nil, errors.New("data null atau error")
}

// GetListActive ambil data
func GetListActive() ([]koneksi.TagListActive, error) {
	var res []koneksi.TagListActive
	var q string

	if koneksi.TypeDatabase == "mysql" {
		q = "SELECT *  from tag_lists  INNER JOIN controller_lists on tag_lists.idnamefromcontroller_tag  = controller_lists.idname_controller   INNER JOIN usbports on usbports.usb_alias = controller_lists.idport_controller    and  usbports.usb_system is NOT NULL INNER JOIN usbtersedias    WHERE usbtersedias.usb_system = usbports.usb_system    and  length(tag_lists.idnamefromcontroller_tag) > 1      AND tag_lists.idregisterfromcontroller_tag <> 0  	AND controller_lists.idactive_controller =true ORDER BY tag_lists.id ASC"

	} else if koneksi.TypeDatabase == "sqlite3" {
		q = "SELECT *  from tag_lists  INNER JOIN controller_lists on tag_lists.idnamefromcontroller_tag  = controller_lists.idname_controller   INNER JOIN usbports on usbports.usb_alias = controller_lists.idport_controller    and  usbports.usb_system is NOT NULL INNER JOIN usbtersedias    WHERE usbtersedias.usb_system = usbports.usb_system    and  length(tag_lists.idnamefromcontroller_tag) > 1      AND tag_lists.idregisterfromcontroller_tag <> 0  	AND controller_lists.idactive_controller =1 ORDER BY tag_lists.id ASC"
	}
	// cnt++
	if err := koneksi.Condb.Raw(q).Scan(&res).Error; err != nil {
		fmt.Println("ada error ", err)
		return nil, err
	}

	if len(res) > 0 {
		return res, nil
	}

	return nil, errors.New("GetListActive :belum ada controller atau tags /tidak ada controller yang aktif / usb ports yang di pilih pada PORT x")
}

//UpdateValueFromRtuRead update nilai dari alat berdasarkan id
func UpdateValueFromRtuRead(id uint, newValue string) error {
	err := koneksi.Condb.Table("tag_lists").Where("id = ?", id).Updates(koneksi.TagList{IdlastvalueTag: newValue, UpdatedAt: time.Now()})
	if err != nil {
		return nil
	}
	return err.Error
}

//UpdateValueFromTcpWrite update nilai dari alat berdasarkan addres write
func UpdateValueFromTcpWrite(address uint16, newValue string) error {
	err := koneksi.Condb.Table("tag_lists").Where("idregistertocontroller_tag = ?", address).Updates(koneksi.TagList{IdlastvalueTag: newValue, UpdatedAt: time.Now()})
	// err := koneksi.Condb.Table("tag_lists").Where("id = ?", id).Updates(TagList{IdlastvalueTag: newValue, UpdatedAt: time.Now()})
	if err != nil {
		return nil
	}
	return err.Error
}
